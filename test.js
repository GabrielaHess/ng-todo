// co zostanie wypisane? (założyć że skrypt uruchomiony przez node v5.4.0)

console.log('\n--- 1');
(function() {
  var a;
  console.log(a);
})();

console.log('\n--- 2');
console.log((() => {})());

console.log('\n--- 3');
console.log({}.a);

console.log('\n--- 4');
((a) => console.log(a))();

console.log('\n--- 5');
console.log(null == undefined);

console.log('\n--- 6');
console.log(null === undefined);

console.log('\n--- 7');
console.log(typeof null);
console.log(typeof undefined);

console.log('\n--- 8');
(function() {
  const a = {a: 1};
  const b = {a: 1};
  console.log(a == b);
  console.log(a === a);
  console.log(a === b);
})();

console.log('\n--- 9');
console.log('false' == false);
console.log('true' === true);

console.log('\n--- 10');
console.log('' == true);
console.log(new String('') == true);

console.log('\n--- 11');
console.log(Boolean( () => {} ) === true);
console.log(Boolean( (() => {})() ) === true);
console.log(!!Boolean( (x => !x)(0) ) == 1);

console.log('\n--- 12');
console.log([] == true);
if ([]) console.log('soo true');
Array.prototype.valueOf = () => true;
console.log([] == false);
if (![]) console.log('sooo true');

console.log('\n--- 13');
(function() {
  function Person(name) {
    this.name = name;
  }

  Person.prototype.introduce = function(greeting) {
    console.log(greeting + ', my name is ' + this.name);
  };

  const persons = ['Henio', 'Mati', 'Seba'].map(name => new Person(name))
  const introduce = persons[2].introduce.bind(persons[1]);

  persons[0].introduce('Hello');
  persons[1].introduce.call({ name: 'Kuba' }, 'Ugabuga');
  persons[1].introduce.apply(persons[0], ['Hi']);
  introduce('Elo');
})();

console.log('\n--- 14');
console.log(typeof []);

console.log('\n--- 15');
console.log(1 + 2 + '3');

console.log('\n--- 16');
console.log('3' + 2 + 1);

console.log('\n--- 17');
(function() {
  const a = +'dupa';
  console.log(a);
  console.log(a === a);
})();

console.log('\n--- 18');
(function() {
  var y = 1, x = y = typeof x;
  console.log(x);
})();

console.log('\n--- 19');
(function() {
  var a = 2, b = true, c = 20;
  console.log(a && b && c);
})();

console.log('\n--- 20');
(function() {
  var foo = 'outside';
  function logIt() {
    console.log(foo);
    var foo = 'inside';
  }
  logIt();
})();

console.log('\n--- 21');
(function() {
  try {
    const a = 42..toString();
    console.log(a);
    const b = 42 . toString();
    console.log(b);
  } catch(e) {
    console.log('Nice try...');
  }
})();

console.log('\n--- 22');
console.log(typeof(NaN));

console.log('\n--- 23');
(function() {
  var a = 1;
  function b() {
      a = 10;
      return;
      function a() {}
  }
  b();
  console.log(a);
})();

console.log('\n--- 24');
(function() {
  var myObject = {
    price: 20.99,
    get_price : function() {
      return this.price;
    }
  };
  var customObject = Object.create(myObject);
  customObject.price = 19.99;

  delete customObject.price;
  console.log(customObject.get_price());
})();

console.log('\n--- 25');
(function() {
  var num = 10,
      name = "Addy Osmani",
      obj1 = {
        value: "first value"
      },
      obj2 = {
        value: "second value"
      },
      obj3 = obj2;

  function change(num, name, obj1, obj2) {
      num = num * 10;
      name = "Paul Irish";
      obj1 = obj2;
      obj2.value = "new value";
  }

  change(num, name, obj1, obj2);

  console.log(num);
  console.log(name);
  console.log(obj1.value);
  console.log(obj2.value);
  console.log(obj3.value);
})();

console.log('\n--- 26');
(function() {
  function foo() {
    return
    {
    };
  }
  console.log(foo());
})();


console.log('\n--- 27');
(function() {
  console.log(a);
  console.log(foo());
  var a = 1;
  function foo() {
    return 2;
  }
})();

console.log('\n--- 28');
(function() {
  var data = [0, 1, 2];
  var funcs = [];

  function init() {
    for (var i = 0; i < 3; i++) {
      var x = data[i];
      var innerFunc = function() {
          return x;
      };
      funcs.push(innerFunc);
    }
  }

  function run() {
    for (var i = 0; i < 3; i++) {
      console.log(data[i] + ", " +  funcs[i]());
    }
  }

  init();
  run();
})();

console.log('\n--- 29');
(function() {
  var myObject = {
    myCountryName: 'Poland',
    getMyCountryName: function () {
      return this.myCountryName;
    }
  };

  var countryInfo = myObject.getMyCountryName;

  console.log(countryInfo());
  console.log(myObject.getMyCountryName());
})();

console.log('\n--- 30');
(function() {
  var name = "Seba";
  (function() {
    console.log(name);
    var name = "Mati";
    console.log(name);
  })();
})();

console.log('\n--- 31');
(function() {
  var count = 1;
  if (function tempFunc(){}) {
    count += typeof tempFunc;
  }
  console.log(count);
})();

console.log('\n--- 32');
(function(){
  if(true) {
    function innerFunc() {
      console.log("innerFunc: Inside if");
    }
    var innerFuncExpr = function () {
      console.log("innerFuncExpr: Inside if");
    }
  } else {
    function innerFunc() {
      console.log("innerFunc: Inside else");
    }
    var innerFuncExpr = function () {
      console.log("innerFuncExpr: Inside else");
    }
  }

  innerFunc();
  innerFuncExpr();
})();

console.log('\n--- 33');
(function(){
  console.log([2,4,6,8,10].map(parseInt));
})();

console.log('\n--- 34');
(function(){
  function func() {
    return varOrFunc;
    varOrFunc = 1;

    function varOrFunc() {
        console.log("Inside varOrFunc")
    }
    var varOrFunc = '2';
  }
  console.log(typeof func());
})();

console.log('\n--- 35');
(function(){
  var myObject = {
    myName: "Minkowsky",
    myFunc: function() {
      var self = this;
      console.log(this.myName);
      console.log(self.myName);
      (function() {
          console.log(this.myName);
          console.log(self.myName);
      }());
    }
  };
  myObject.myFunc();
})();

console.log('\n--- 36');
(function(){
  var root = {};
  var elementOne = { key: '1'};
  var elementTwo = { key: '2'};

  root[elementOne] = "Element One";
  root[elementTwo] = "Element Two";

  console.log(root[elementOne]);
})();
