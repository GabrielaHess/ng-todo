(function () {

  var myList = angular.module('myToDo', ["ui.router"]);


  myList.config(config)

  function config($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise(function ($injector) {
			var $state = $injector.get("$state");
			$state.go("list");
		});


    $stateProvider
      .state('list', {
        url: '/list',
        templateUrl: "templates/list.html",
        controller: 'ListController',
        controllerAs: 'list'
      })
      .state('create', {
        url: '/create',
        templateUrl: "templates/create.html",
        controller: 'CreateController',
        controllerAs: 'create'
      });
  }
})();

