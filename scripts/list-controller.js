(function () {
    var myList = angular.module('myToDo');

    myList.controller('ListController', ['httpServices', function (httpServices) {
        var list = this;
        list.todos = [];


        httpServices.getItemsFromServer()
            .then(function (response) {
                list.todos = response.data;
            });


        this.updateItem = function (todo) {
            var id = todo.id
            httpServices.updateItemsOnServer(todo, id);
        };

        this.removeItem = function (todo, index, onlyJson) {
            var id = todo.id
            httpServices.removeItemsFromServer(id);

            if (!onlyJson) {
                list.todos.splice(index, 1)
            }
        };

        this.clearCompleted = function () {
            // console.log('blaaa')
            // console.log(list.todos)
            var activeTasks = [];
            angular.forEach(list.todos, function (todo, i) {
                if (todo.done === 'true') {
                    list.removeItem(todo, i, true);
                    // console.log('bla')
                }
                else {
                    activeTasks.push(todo);
                }
            });
            list.todos = activeTasks;
        };

    }]);
})();

