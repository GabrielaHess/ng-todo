(function () {
    'use strict';

    var myList = angular.module('myToDo');

    myList.factory('httpServices', httpServices);
    httpServices.$inject = ['$http'];

    function httpServices($http) {
        return {
            getItemsFromServer: getItemsFromServer,
            updateItemsOnServer: updateItemsOnServer,
            removeItemsFromServer: removeItemsFromServer,
            addNewItemsToServer: addNewItemsToServer
        };

        function getItemsFromServer() {
            return $http.get('http://localhost:3000/todolist');
        }

        function updateItemsOnServer(todo, id) {
            return $http({
                method: "PUT",
                url: 'http://localhost:3000/todolist/' + id,
                data: todo
            });
        }

        function removeItemsFromServer(id) {
            var itemUrl = 'http://localhost:3000/todolist/' + id;
            return $http.delete(itemUrl)
        }

        function addNewItemsToServer(newItem) {
            return $http.post('http://localhost:3000/todolist', newItem);
        }

    }


})();