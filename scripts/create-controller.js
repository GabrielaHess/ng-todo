(function () {
    var myList = angular.module('myToDo');

    myList.controller('CreateController', ['httpServices',  function (httpServices) {
        // newThing = '';
        this.addNew = function (newThing) {
            // console.log(newThing);
            var newTodo = {
                item: newThing,
                done: false
            };
            // console.log(newTodo)
            httpServices.addNewItemsToServer(newTodo);
            this.newThing = '';
            // console.log(newThing)
        };

    }]);

})();
